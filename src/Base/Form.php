<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 03/12/2014
 * Time: 17:58
 */

namespace Skimia\Form\Base;

use Config;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use ReflectionClass;

use View;
class Form {

    protected $template = false;

    protected $fields = [];

    protected $data = false;

    protected $data_handled = null;

    protected $additional_info = false;

    public function __construct($data = false){

        $this->data = $data;

        if(!is_object($this->fields))
            $this->fields = new Collection($this->fields);

        //$this->makeFields();
    }

    protected $fieldsMaked = false;
    protected function makeFields(){
        $this->fieldsMaked = true;
        return $this;
    }

    public function makeFieldsIfNotMaked(){
        if(!$this->fieldsMaked)
            $this->makeFields();
        return $this;
    }
    /**
     * par default $file omis cherche dans le dossier actuel le underscorize() du nom de la classe .yml
     * @param bool $file chemin du fichier
     */
    public function appendFromYaml($file = false){

        //get array from yaml

        //return $this->appendFromArray($array);
    }

    /**
     * par default $file omis cherche dans le dossier actuel le underscorize() du nom de la classe .php
     * @param bool $file chemin du fichier
     * @return Form
     */
    public function appendFromFile($file = false){
        if($file === false){
            $file = $this->getFileName('fields.php');
        }

        $array = include_once($file);
        //verifier si array est bien rempli
        return $this->appendFromArray($array);
    }

    public function appendFromArray($fields){
        if(!is_array($fields))
            throw new \Exception('Form::appendFromArray $fields doit être un array');

        foreach($fields as $name=>$field){

            if(empty($name)){
                if(isset($field['name'])){
                    $name = $field['name'];
                    unset($field['name']);
                }else
                    throw new \Exception('Form::appendFromArray $fields un des champs n\'est pas defini');
            }

            if(!isset($field['type']))
                throw new \Exception('Form::appendFromArray $fields le champ "'.$name.'" n\'as pas de type');

            $type = $field['type'];
            unset($field['type']);

            $info = false;
            $label = false;

            if(isset($field['label'])) {
                $label = $field['label'];
                unset($field['label']);
            }

            if(isset($field['info'])) {
                $info = $field['info'];
                unset($field['info']);
            }


            $this->add($name,$type,$label,$info,$field);
        }
        return $this;
    }

    public function add($name, $type, $label = false, $info = false, $options = false){
        if($options === false){
            $options = [];
        }

        $options = new Collection($options);

        if($label !==false){
            $options['label'] = $label;
        }

        if($info !==false){
            $options['info'] = $info;
        }
        $options['type'] = $type;
        $options['name'] = $name;

        $this->fields[$name] = $options;

        return $this;

    }



    protected function handle(){
        $this->makeFieldsIfNotMaked();
        //recup la request
        $this->data_handled = new Collection();
        foreach(array_keys($this->fields->all()) as $name){
            if(Input::has($name)){
                $value = Input::get($name);
                $this->afterHandleField($name,$value);
                $this->data_handled->put($name,$value);

            }

        }
        //false si vide
        if($this->data_handled->count() == 0){
            $this->data_handled = false;
            return false;
        }
        return true;
    }

    protected function afterHandleField($key,&$value){

    }

    public function isSubmitted(){
        if(null === $this->data_handled){
            if($this->handle() === false)
                return false;
        }elseif(false === $this->data_handled){
            return false;
        }
        return true;
    }
    public function isValid($supp_validation = false){

        if(!$this->isSubmitted()){
            return false;
        }

        $rules = [];

        foreach($this->fields as $name=>$field){
            if(isset($field['rules']) && is_array($field['rules'])){
                $rules[$name] = $field['rules'];
                if(!in_array('required',$field['rules']) && (isset($field['required']) && $field['required'] === true))
                    $rules[$name][] = 'required';

            }elseif((isset($field['required']) && $field['required'] === true) || in_array('required',is_array($field)?$field: $field->toArray(),true))
                $rules[$name] = ['required'];
        }

        if(is_array($supp_validation)){
            foreach($supp_validation as $name => $validation){
                if(!isset($rules[$name]))
                    $rules[$name] = $validation;
                else
                    $rules[$name] = array_merge($rules[$name],$supp_validation);
            }
        }
       /* var_dump($this->data_handled->all());*/

        $validator = \Validator::make(
            $this->data_handled->all(),
            $rules
        );

        if($validator->fails()){

            $messages = $validator->messages();

            //ajouter aux fields les erreurs trouvées
            //pour les afficher dans le rendu du formulaire
            $i = 1;
            $fields = $this->fields->all();
            foreach($this->fields as $name=>$field){
                if($messages->has($name)){
                    $fields[$name]['errors']=$messages->get($name);
                    \AResponse::addMessage($messages->get($name),'error',$i * 5000);
                    $i++;
                }
            }
            $this->fields = new Collection($fields);

            return false;
        }else
            return true;
    }

    public function __get($name){

        if(!$this->isSubmitted()){
            throw new \Exception('Impossible de faire cela. Aucune valeur n\'a été posté Utiliser Form->isValid() avant');
        }

        if(isset($this->data_handled[$name]))
            return $this->data_handled[$name];
        else
            return null;
    }
    public function all(){
        if(!$this->isSubmitted()){
            throw new \Exception('Impossible de faire cela. Aucune valeur n\'a été posté Utiliser Form->isValid() avant');
        }
        return $this->data_handled;
    }

    public function make($view = false, $options = []){
        $this->makeFieldsIfNotMaked();
        if(!isset($options['fields']))
            $options['fields'] = $this->fields->all();

        if($this->isSubmitted()){
            foreach($options['fields'] as $name => &$value) {
                if($this->data_handled->has($name))
                    $value['value'] = $this->data_handled[$name];
                else{
                    $value['value'] = $this->getOldData($name);
                    if(empty($value['value']) && isset($value['default'])){
                        $value['value'] = isset($value['default'])?$value['default']:'';
                    }
                }

            }
        }else
            foreach($options['fields'] as $name => &$value) {
                $value['value'] = $this->getOldData($name);
                if(empty($value['value']) && isset($value['default'])){
                    $value['value'] = isset($value['default'])?$value['default']:'';
                }

                if(!isset($value['label']))
                    $value['label'] = $name;
            }

        \Debugbar::info($options);
        $options['form'] = $this;
        $options['additional_info'] = $this->additional_info;

        return View::make(is_string($view) && !empty($view) ? $view : $this->getTemplate(), $this->makeVars($options));
    }

    /**
     * @return array
     */
    protected function getData(){
        return $this->data;
    }

    protected function getOldData($name){
        return $this->getRawOldData($name);

    }
    protected function getRawOldData($name){
        $this->makeFieldsIfNotMaked();
        if(is_array($this->data)){
            if(isset($this->data[$name]) && !empty($this->data[$name]))
                return $this->data[$name];
            if(isset($this->fields[$name]['default']))
                return $this->fields[$name]['default'];
            return '';
        }
        else if(is_object($this->data)){
            if(method_exists($this->data,$name)){
                $method = $name;
                $data =  $this->data->$method();

                if(empty($data) && isset($this->fields[$name]['default']))
                    return $this->fields[$name]['default'];
                return $data;
            }
            else if(method_exists($this->data,'get'.ucfirst($name))){
                $method = 'get'.ucfirst($name);
                $data =  $this->data->$method();
                if(empty($data) && isset($this->fields[$name]['default']))
                    return $this->fields[$name]['default'];
                return $data;
            }else if(isset($this->data->$name) && !empty($this->data->$name))
                return $this->data->$name;
            if(isset($this->fields[$name]['default']))
                return $this->fields[$name]['default'];
            return '';
        }
        elseif($this->data === false){
            if(isset($this->fields[$name]['default']))
                return $this->fields[$name]['default'];
            return '';
        }
        else
            throw new \Exception('impossible d\'annalyser le type '.gettype($this->data));
    }


    protected function makeVars($merged){

        return $merged;
    }
    /***************************************/
    /* HelperFunctions*/
    protected function getFileName($extension = false){
        $obj = new ReflectionClass($this);
        if($extension === false)
            return $obj->getFileName();
        else{
            return str_replace('.'.$obj->getExtensionName(),'.'.$extension,$obj->getFileName());
        }
    }

    public function getTemplate(){
        if($this->template === false){
            $this->template = Config::get('skimia.form::templates.global');
        }
        return $this->template;
    }

    public static function render($additional_data = [],$template = false){
        $instance = new self();
        return $instance->make($template,$additional_data)->render();
    }

    public static function getChoices($field){

        if(isset($field['choices']))
            return $field['choices'];
        elseif(isset($field['choicesFct'])){
            return $field['choicesFct']($field);
        }else{
            dd($field);
            return [];
        }
    }



} 