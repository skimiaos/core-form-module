<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 03/12/2014
 * Time: 17:58
 */

namespace Skimia\Form\Base;


use Illuminate\Database\Eloquent\Model;

class EntityForm extends Form {

    protected $langKey = 'change.me::form';

    protected $hiddenFields =  ['created_at','deleted_at','updated_at','id'];
    /**
     * @var Eloquent
     */
    protected $data = null;
    public function __construct(Model $model){
        $this->data = $model;

        parent::__construct($model);
    }

    protected function makeFields(){
        $schema = \Schema::getColumnListing($this->data->getTable());
        $schema = array_diff($schema,$this->data->getGuarded());
        $schema = array_diff($schema,$this->hiddenFields);

        foreach($schema as $field){
            if(!$this->fields->has($field))
                $this->add($field,'text',trans($this->langKey.'.'.$field.'.label'),trans($this->langKey.'.'.$field.'.info'));
        }

    }

    public function saveEntity(){
        $this->makeEntity()->save();
    }

    protected function makeEntity(){
        if(is_object($this->data)){
            foreach($this->fields as $key=>$definition){

                $dataValue = false;

                if(isset($this->data_handled[$key]))
                    $dataValue = $this->data_handled[$key];

                if(isset($definition['formToE']) && is_callable($definition['formToE'])){
                    $clean = false;
                    $ref_data = $this->data->$key;

                    if(is_null($ref_data)){
                        $ref_data = $dataValue;
                        $clean = true;
                    }

                    $newValue = $definition['formToE']($this->data,$ref_data,$key,$definition);

                    if(!$clean && !(isset($definition['mapped']) && $definition['mapped']  === false))
                        $this->data->$key = $ref_data;

                    if(!(isset($definition['mapped']) && $definition['mapped']  === false)){
                        $this->data->$key = $newValue;
                    }

                }

                if(isset($definition['mapped']) && $definition['mapped']  === false){

                }
                else if($dataValue !== false || $definition['type'] =='checkbox'){

                    if(\FieldHelper::hasViewDataTransformer($definition['type'])){
                        \FieldHelper::TransformViewToData($definition['type'],[&$this->data,&$dataValue,$key,$definition,$this],true);

                    }elseif(method_exists($this->data,'_set'.ucfirst($key))){
                        $method = '_set'.ucfirst($key);
                        $this->data->$method($dataValue);
                    }elseif(method_exists($this->data,'set'.ucfirst($key))){
                        $method = 'set'.ucfirst($key);
                        $this->data->$method($dataValue);
                    }else
                        $this->data->$key = $dataValue;
                }



            }
            return $this->data;
        }

    }

    protected function getOldData($name){
        $data = $this->getRawOldData($name);

        if(is_object($data)){

            if(is_a($data,'Illuminate\Database\Eloquent\Relations\BelongsToMany')){
                $field = $this->fields[$name];
                //dd($data->getRelationName());

                $primaryKey = 'id';
                if(isset($field['primary_key']))
                    $primaryKey = $field['primary_key'];

                if(isset($field['show_collumn_mapped']) && $field['show_collumn_mapped'] == false){
                    $db_fields = ['*'];
                }else{
                    if(is_array($field['show_collumn']))
                        $db_fields = $field['show_collumn'];
                    else
                        $db_fields = [$field['show_collumn']];

                    $db_fields[] =$data->getRelated()->getTable() .'.id';
                }


                $collection = $data->get($db_fields);
                $col2 = [];
                if(is_array($field['show_collumn'])){
                    $f = $field['show_collumn'][0];

                    foreach($collection as $k=>$item){

                        $e = '';
                        foreach($field['show_collumn'] as $coll){

                            if(method_exists($item,'get'.ucfirst($coll))){
                                $meth = 'get'.ucfirst($coll);
                                $e = $e . $item->$meth() . ' ';
                            }else
                                $e = $e . $item[$coll].' ';

                        }
                        $e = trim($e);

                        $item = $item->toArray();
                        $item[$f] = $e;
                        $col2[$k] = $item;
                    }
                }

                array_walk($col2,function(&$item){
                    unset($item['pivot']);
                });
                return $col2;
            }
        }

        return $data;

    }

    /**
     * @return array
     */
    protected function getData(){
        if(isset($this->data) && !is_null($this->data))
            return $this->data->toArray();
        else
            return [];
    }
} 