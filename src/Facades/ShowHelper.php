<?php

namespace Skimia\Form\Facades;

use \Illuminate\Support\Facades\Facade;

class ShowHelper extends Facade{

    protected static function getFacadeAccessor(){
        return 'show.viewDataHelper';
    }
}