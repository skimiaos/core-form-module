<?php

namespace Skimia\Form\Facades;

use \Illuminate\Support\Facades\Facade;

class FieldHelper extends Facade{

    protected static function getFacadeAccessor(){
        return 'form.viewDataHelper';
    }
}