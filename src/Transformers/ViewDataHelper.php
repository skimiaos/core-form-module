<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/04/2015
 * Time: 16:44
 */

namespace Skimia\Form\Transformers;


class ViewDataHelper extends ViewDataTransformer {

    public function render($type, $name = '', $field = [], $options = []){
        if(isset($field['_display']) && !$field['_display'])
            return '';
        if($this->viewTemplates->offsetExists($type))
        {
            if((isset($field['disabled']) && $field['disabled'] == true)
                || in_array('disabled',(is_object($field) ? $field->toArray():$field),true))
                $field['dsbl'] = 'disabled';
            else
                $field['dsbl'] = '';

            if(isset($field['template']) && $field['template'] !== false){
                $data = ['name'=>$name,'field'=>$field,'offset'=>$type];

                return \View::make( $field['template'] ,$data ,$options )->render();

            }else
                return $this->renderTemplate($type,['name'=>$name,'field'=>$field],$options);
        }

        else
            return 'le champ de type="'.$type.'" est Introuvable';
    }

}