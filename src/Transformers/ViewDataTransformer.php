<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/04/2015
 * Time: 16:44
 */

namespace Skimia\Form\Transformers;


class ViewDataTransformer {

    protected $dataViewTransformers = null;
    protected $viewDataTransformers = null;
    protected $viewTemplates = null;

    public function __construct(){

        $this->dataViewTransformers = new PriorityArray();
        $this->viewDataTransformers = new PriorityArray();
        $this->viewTemplates = new PriorityArray();
    }


    public function setTemplate($offset, $value ,$priority = false){
        $this->viewTemplates->set($offset,$value,$priority);
    }
    public function setViewDataTransformer($offset, $value ,$priority = false){
        $this->viewDataTransformers->set($offset,$value,$priority);
    }
    public function setDataViewTransformer($offset, $value ,$priority = false){
        $this->dataViewTransformers->set($offset,$value,$priority);
    }


    public function hasTemplate($offset){
        return $this->viewTemplates->offsetExists($offset);
    }
    public function hasViewDataTransformer($offset){
        return $this->viewDataTransformers->offsetExists($offset);
    }
    public function hasDataViewTransformer($offset){
        return $this->dataViewTransformers->offsetExists($offset);
    }

    public function getTemplateMaxPriority($offset){
        return $this->viewTemplates->getMaxPriority($offset);
    }
    /**
     * @param $offset
     * @return \Closure
     */
    public function getViewDataTransformerMaxPriority($offset){
        return $this->viewDataTransformers->getMaxPriority($offset);
    }

    /**
     * @param $offset
     * @return \Closure
     */
    public function getDataViewTransformerMaxPriority($offset){
        return $this->dataViewTransformers->getMaxPriority($offset);
    }

    public function renderTemplate($offset, $data = [], $mergeData = []){

        $template = $this->viewTemplates->offsetGet($offset);

        $data = array_merge($data,['offset'=>$offset]);

        return \View::make($template,$data,$mergeData)->render();
    }

    public function TransformDataToView($offset, &$data, $func_array = false){
        if($this->dataViewTransformers->offsetExists($offset)){

            /**
             * @var $closure \Closure
             */
            $closure = $this->dataViewTransformers->offsetGet($offset);
            if($func_array)
                return call_user_func_array($closure,$data);
            else
                return $closure($data);
        }
        else
            return $func_array ? $data[1] : $data;

    }

    public function TransformViewToData($offset, &$data, $func_array = false){
        if($this->viewDataTransformers->offsetExists($offset)){
            /**
             * @var $closure \Closure
             */
            $closure = $this->viewDataTransformers->offsetGet($offset);

            if($func_array)
                return call_user_func_array($closure,$data);
            else
                return $closure($data);
        }
        else
            return $data;

    }
}