<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 10/04/2015
 * Time: 15:31
 */

namespace Skimia\Form\Transformers;


class PriorityArray implements \ArrayAccess{

    protected $defaultPriority = 1000;

    protected $needStrictPriority = false;


    protected $dataArray = [];

    /**
     * @param array|bool $defaultData donnée par defaut mis a la priorité par defaut
     * @param int $defaultPriority priorité par defaut
     * @param bool $needStrictPriority
     */
    public function __construct($defaultData = false, $defaultPriority = 1000, $needStrictPriority = false){

        if(is_array($defaultData))
            foreach ($defaultData as $offset => $value) {
                $this->dataArray[$offset] = [$defaultPriority=>$value];
            }

        $this->defaultPriority = $defaultPriority;
        $this->needStrictPriority = $needStrictPriority;

    }



    public function set($offset, $value ,$priority = false){
        if($priority == false) {
            $this->offsetSet($offset,$value);
        }else{

            if($this->offsetExists($offset))
                $this->dataArray[$offset][$priority] = $value;
            else
                $this->dataArray[$offset] = [$priority=>$value];

        }

        return $this;

    }

    public function getMaxPriority($offset){
        $this->offsetGet($offset);
        return key($this->dataArray[$offset]);
    }

    public function dd(){
        foreach ($this->dataArray as $k => $v) {
            ksort($this->dataArray[$k]);
        }

        dd($this->dataArray);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return isset($this->dataArray[$offset]) && count($this->dataArray[$offset]) > 0;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        ksort($this->dataArray[$offset]);
        return end ( $this->dataArray[$offset] ) ;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if($this->needStrictPriority)
            throw new \InvalidArgumentException('Fonction désactivé par le parmètre 3:$needStrictPriority Il faut utiliser la fonction set($offset,$value,$priority)');
        if(!$this->offsetExists($offset))
            $this->dataArray[$offset][$this->defaultPriority] = $value;
        else
            $this->dataArray[$offset][$this->getMaxPriority($offset)+1] = $value;

    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset ( $this->dataArray[$offset] ) ;

        return $this;
    }
}