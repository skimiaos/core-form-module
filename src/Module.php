<?php

namespace Skimia\Form;

use Blade;
use Skimia\Modules\ModuleBase;

class Module extends ModuleBase{

    public function beforeRegisterModules(){

        $formViewDataHelper = new Transformers\ViewDataHelper();
        $showViewDataHelper = new Transformers\ViewDataHelper();

        \App::singleton('form.viewDataHelper',function() use ($formViewDataHelper){
            return $formViewDataHelper;
        });

        \App::singleton('show.viewDataHelper',function() use ($showViewDataHelper){
            return $showViewDataHelper;
        });
    }

    public function register(){


        parent::register();
    }

    public function getAliases(){
        return [
            'FieldHelper' => 'Skimia\Form\Facades\FieldHelper',
            'ShowHelper' => 'Skimia\Form\Facades\ShowHelper',
        ];
    }
} 