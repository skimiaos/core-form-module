@block('form.before')


@endshow
@block('form.start')
    {{ Form::open(array('url' => $action,'method'=>$method)) }}
@endshow
@block('form.start.after')

@endshow
@block('form.content')
    @foreach($fields as $name=>$field)
        @if(!isset($field['display']) || $field['display'] !== false)
            @block('field.'.$name.'.row.before')@endshow
            @block('form.row')
                @block('form.row.label')
                    @if(isset($field['label']))
                        @block('form.row.label.content')
                            {{FieldHelper::render('label',$name, $field);}}
                        @endshow
                    @endif
                @endshow

                @block('form.row.widget')
                    @block('form.row.widget.content')
                        @block('form.row.widget.content.'.$field['type'])
                            {{FieldHelper::render($field['type'],$name, $field);}}
                        @endshow
                    @endshow
                @endshow

                @block('form.row.errors')
                    @if(isset($field['errors']))
                        @foreach($field['errors'] as $error)
                            @block('form.row.errors.content')<p>{{$error}}</p>@endshow
                        @endforeach
                    @endif
                @endshow
            @endshow
            @block('field.'.$name.'.row.after')@endshow
        @endif
    @endforeach
@endshow

    @block('form.submit.before')@endshow
    @block('form.submit')
        {{FieldHelper::render('submit');}}
    @endshow

@block('form.end.before')

@endshow
@block('form.end')
{{ Form::close() }}
@endshow

@block('form.end.after')
@endshow

@block('form.after')

@endshow