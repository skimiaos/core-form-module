<?php

return [
    'name'        => 'Module Forms',
    'author'      => 'Skimia',
    'description' => 'Fournit des outils pour définir des formulaires, les afficher, valider et traiter la donnée. peut se lier sur une entitée pour la gestion automatisée de celle-ci.',
    'namespace'   => 'Skimia\\Form',
    'require'     => ['skimia.modules','skimia.blade']
];