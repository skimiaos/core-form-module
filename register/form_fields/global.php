<?php


\FieldHelper::setTemplate('text','skimia.form::fields.text',1000);
\FieldHelper::setTemplate('password','skimia.form::fields.password',1000);
\FieldHelper::setTemplate('checkbox','skimia.form::fields.checkbox',1000);
\FieldHelper::setTemplate('select','skimia.form::fields.select',1000);
\FieldHelper::setTemplate('submit','skimia.form::fields.submit',1000);